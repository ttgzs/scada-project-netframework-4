﻿using SCADA_Core.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SCADA_Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AlarmDisplayService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AlarmDisplayService.svc or AlarmDisplayService.svc.cs at the Solution Explorer and start debugging.
    public class AlarmDisplayService : IAlarmDisplay
    {
        private static readonly object proxyLock = new object();
        private static readonly List<IAlarmDisplayCallback> Proxies = new List<IAlarmDisplayCallback>();

        private IAlarmDisplayCallback AlarmDisplayProxy
        {
            get
            {
                return OperationContext.Current.GetCallbackChannel<IAlarmDisplayCallback>();
            }
        }

        public void ConnectToService()
        {
            Proxies.Add(AlarmDisplayProxy);
        }

        public static void SendAlarms(string message, int repeat)
        {
            lock (proxyLock)
            {
                foreach (IAlarmDisplayCallback p in Proxies)
                {
                    p.DisplayAlarm(message, repeat);
                }
            }
        }
    }
}
