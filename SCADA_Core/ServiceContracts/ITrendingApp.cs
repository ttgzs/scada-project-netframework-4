﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SCADA_Core.ServiceContracts
{
    [ServiceContract(CallbackContract = typeof(ITrendingAppCallback), SessionMode = SessionMode.Required)]
    public interface ITrendingApp
    {
        [OperationContract]
        void ConnectToService();
    }

    public interface ITrendingAppCallback
    {
        [OperationContract(IsOneWay = true)]
        void TagValueChanged(string message);
    }
}
