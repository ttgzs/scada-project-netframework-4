﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SCADA_Core.ServiceContracts
{
    [ServiceContract(CallbackContract = typeof(IAlarmDisplayCallback), SessionMode = SessionMode.Required)]
    interface IAlarmDisplay
    {
        [OperationContract(IsInitiating = true)]
        void ConnectToService();
    }

    interface IAlarmDisplayCallback
    {
        [OperationContract(IsOneWay = true)]
        void DisplayAlarm(string message, int repeat);
    }
}
