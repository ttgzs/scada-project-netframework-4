﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SCADA_Core.ServiceContracts
{
    [ServiceContract]
    interface IRTU
    {
        [OperationContract]
        bool SignUp(char address);
        [OperationContract]
        string GetId(char address);
        [OperationContract]
        void ExportPublicKey(string KEY_PATH, string Id);
        [OperationContract]
        void SendMessage(string Id, char address, string message, byte[] signature);
    }
}
