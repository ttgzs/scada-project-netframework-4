﻿using SCADA_Core.Model;
using SCADA_Core.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SCADA_Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ReportManagerService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ReportManagerService.svc or ReportManagerService.svc.cs at the Solution Explorer and start debugging.
    public class ReportManagerService : IReportManager
    {
        public List<AlarmEvent> GetAlarmsFromInterval(DateTime from_, DateTime to)
        {
            lock (ContextDataHandler.ACLock)
            {
                ContextDataHandler.AC = new AlarmEventContext();
                if (!ContextDataHandler.AC.Database.Exists())
                {
                    ContextDataHandler.AC.Dispose();
                    return new List<AlarmEvent>();
                }
                IEnumerable<AlarmEvent> retval = from ae in ContextDataHandler.AC.Alarms
                                                 where (from_ <= ae.TimeStamp && ae.TimeStamp <= to)
                                                 select ae;
                retval.OrderBy(a => a.Priority).ThenByDescending(a => a.TimeStamp);
                return retval.ToList();
            }
            
        }

        public List<AlarmEvent> GetAlarmsWithPriority(int priority)
        {
            lock (ContextDataHandler.ACLock)
            {
                ContextDataHandler.AC = new AlarmEventContext();
                if (!ContextDataHandler.AC.Database.Exists())
                {
                    ContextDataHandler.AC.Dispose();
                    return new List<AlarmEvent>();
                }
                IEnumerable<AlarmEvent> retval = from ae in ContextDataHandler.AC.Alarms
                                                 where ae.Priority == priority
                                                 select ae;
                retval.OrderByDescending(a => a.TimeStamp);
                return retval.ToList();
            }
        }

        public List<ObservedTagData> NewestAITagValues()
        {
            lock (ContextDataHandler.TCLock)
            {
                ContextDataHandler.TC = new TagDataContext();
                if (!ContextDataHandler.TC.Database.Exists())
                {
                    ContextDataHandler.TC.Dispose();
                    return new List<ObservedTagData>();
                }
                string analogueInput = typeof(AnalogueInput).ToString();
                IEnumerable<ObservedTagData> retval = from t in ContextDataHandler.TC.TagData
                                                      where (t.TagType == analogueInput)
                                                      group t by t.TagName into sameTags
                                                      let newestAITagValue = sameTags.Max(tag => tag.TimeStamp)
                                                      select sameTags.FirstOrDefault(tag => tag.TimeStamp == newestAITagValue);
                retval.OrderByDescending(a => a.TimeStamp);
                return retval.ToList();
            }
        }

        public List<ObservedTagData> NewestDITagValues()
        {
            lock (ContextDataHandler.TCLock)
            {
                ContextDataHandler.TC = new TagDataContext();
                if (!ContextDataHandler.TC.Database.Exists())
                {
                    ContextDataHandler.TC.Dispose();
                    return new List<ObservedTagData>();
                }
                string digitalInput = typeof(DigitalInput).ToString();
                IEnumerable<ObservedTagData> retval = from t in ContextDataHandler.TC.TagData
                                                      where (t.TagType == digitalInput)
                                                      group t by t.TagName into sameTags
                                                      let newestDITagValue = sameTags.Max(tag => tag.TimeStamp)
                                                      select sameTags.FirstOrDefault(tag => tag.TimeStamp == newestDITagValue);
                retval.OrderByDescending(a => a.TimeStamp);
                return retval.ToList();
            }
        }

        public List<ObservedTagData> TagValuesFromInterval(DateTime from_, DateTime to)
        {
            lock (ContextDataHandler.TCLock)
            {
                ContextDataHandler.TC = new TagDataContext();
                if (!ContextDataHandler.TC.Database.Exists())
                {
                    ContextDataHandler.TC.Dispose();
                    return new List<ObservedTagData>();
                }
                IEnumerable<ObservedTagData> retval = from t in ContextDataHandler.TC.TagData
                                             where (from_ <= t.TimeStamp && t.TimeStamp <= to)
                                             select t;
                retval.OrderByDescending(a => a.TimeStamp);
                return retval.ToList();
            }
        }

        public List<ObservedTagData> AllTagValues(int tagName)
        {
            lock (ContextDataHandler.TCLock)
            {
                ContextDataHandler.TC = new TagDataContext();
                if (!ContextDataHandler.TC.Database.Exists())
                {
                    ContextDataHandler.TC.Dispose();
                    return new List<ObservedTagData>();
                }
                IEnumerable<ObservedTagData> retval = from t in ContextDataHandler.TC.TagData
                                                      where (t.TagName == tagName)
                                                      select t;
                retval.OrderBy(a => a.Value);
                return retval.ToList();
            }
        }

        public List<int> GetTags()
        {
            lock (ContextDataHandler.TCLock)
            {
                ContextDataHandler.TC = new TagDataContext();
                if (!ContextDataHandler.TC.Database.Exists())
                {
                    ContextDataHandler.TC.Dispose();
                    return new List<int>();
                }
                IEnumerable<int> retval = from t in ContextDataHandler.TC.TagData
                                                      select t.TagName;
                return retval.Distinct().ToList();
            }
                 
        }
    }
}
