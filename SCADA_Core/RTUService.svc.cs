﻿using SCADA_Core.ServiceContracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;

namespace SCADA_Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RTUService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select RTUService.svc or RTUService.svc.cs at the Solution Explorer and start debugging.
    public class RTUService : IRTU
    {
        private static readonly object DriversLock = new object();
        private static readonly object KeysLock = new object();
        private static readonly Dictionary<char, string> RTUDrivers = new Dictionary<char, string>();     // <address, id> pair
        private static readonly Dictionary<string, string> PublicKeys = new Dictionary<string, string>(); // <id, keypath> pair
        public static CspParameters csp = new CspParameters();
        public static RSACryptoServiceProvider rsa;
        private static readonly string KEY_STORE = "MY_KEY_STORE";

        public void SignOff() { }

        public bool SignUp(char address)
        {
            lock (DriversLock)
            {
                if (RTUDrivers.ContainsKey(address))
                {
                    return false;
                }
                string newId = GenerateId();
                while (RTUDrivers.ContainsValue(newId)) { newId = GenerateId(); }
                RTUDrivers.Add(address, newId);
                return true;
            }
            
        }
        public string GetId(char address)
        {
            lock (DriversLock)
            {
                return RTUDrivers[address];
            }
            
        }
        private static string GenerateId()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, 8)
                .Select(s => s[new Random().Next(s.Length)]).ToArray());
        }

        public void ExportPublicKey(string KEY_PATH, string Id)
        {
            lock (KeysLock)
            {
                PublicKeys.Add(Id, KEY_PATH);
            }
        }
        private void LoadPublicKey(string Id)
        {
            lock (KeysLock)
            {
                FileInfo fi = new FileInfo(PublicKeys[Id]);
                if (fi.Exists)
                {
                    using (StreamReader reader = new StreamReader(PublicKeys[Id]))
                    {
                        csp.KeyContainerName = KEY_STORE;
                        rsa = new RSACryptoServiceProvider(csp);
                        string publicKeyText = reader.ReadToEnd();
                        rsa.FromXmlString(publicKeyText);
                        rsa.PersistKeyInCsp = true;
                        return;
                    }
                }
            }
        }
        private bool VerifySignedMessage(string message, byte[] signature)
        {
            return rsa.VerifyData(Encoding.UTF8.GetBytes(message), "SHA256", signature);
        }

        public void SendMessage(string Id, char address, string message, byte[] signature)
        {
            LoadPublicKey(Id);
            if (VerifySignedMessage(message, signature))
            {
                double value = double.Parse(message);
                RealTimeDriver.AddValue(address, value);
            }
        }
    }
}
