﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SCADA_Core
{
    public class RealTimeDriver
    {
        private static readonly object rtdlock = new object();
        private static Dictionary<char, double> RTDAddressValues = new Dictionary<char, double>();

        public static void AddValue(char address, double value)
        {
            lock (rtdlock)
            {
                if (RTDAddressValues.ContainsKey(address))
                {
                    RTDAddressValues[address] = value;
                }
                else { RTDAddressValues.Add(address, value); }
            }
        }

        public static double Read(char address)
        {
            lock (rtdlock)
            {
                try
                {
                    return RTDAddressValues[address];
                }
                catch (KeyNotFoundException)
                {
                    return -1;
                }
            }
        }
    }
}