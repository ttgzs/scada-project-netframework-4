﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCADA_Core.Interfaces
{
    interface ITag
    {
        int TagName { get; set; }
        string Description { get; set; }
        char IOAdress { get; set; }

    }
}
