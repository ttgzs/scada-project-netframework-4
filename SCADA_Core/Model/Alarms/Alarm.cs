﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SCADA_Core.Model
{
    [DataContract]
    public class Alarm
    {
        public enum AlarmType
        {
            LOWER,
            HIGHER
        }
        [DataMember]
        public int Priority { get; set; }
        [DataMember]
        public AlarmType Type { get; set; }
        [DataMember]
        public int Threshold { get; set; }
        [DataMember]
        public int Name { get; set; }
        
        public bool IsAlarming(double value)
        {
            return Type == AlarmType.LOWER ? value < Threshold : value > Threshold;
        }
        public string Message { get; private set; }
        public DateTime TimeStamp { get; private set; }
        public void LogAlarm()
        {
            AlarmLogger al = new AlarmLogger();
            al.AppendLog(Message);
        }
        public void GenerateMessage(string unit)
        {
            TimeStamp = DateTime.Now;
            Message = $"Tag {Name} value is {Type} than threshold {Threshold}{unit} at time {TimeStamp}. " +
                $"Alarm's priority is: {Priority}.";
        }
    }
}