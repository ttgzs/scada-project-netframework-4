﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SCADA_Core.Model
{
    public class TagDataContext : DbContext
    {
        public DbSet<ObservedTagData> TagData { get; set; }
    }
}