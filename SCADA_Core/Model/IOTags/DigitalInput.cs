﻿using System;
using SCADA_Core.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace SCADA_Core.Model
{
    [DataContract]
    public class DigitalInput : Input, ITag
    {
        [DataMember]
        public int TagName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public char IOAdress { get; set; }

        public override double Read()
        {
            double retval = SimulationDriver.SimulationDriver.Addresses.Contains(IOAdress) ?
                SimulationDriver.SimulationDriver.ReturnValue(IOAdress) : RealTimeDriver.Read(IOAdress);
            if (retval == -1)
            {
                return -1; // ERROR, nonexisting address in RealTimeDriver.
            }
            return retval > 50 ? 1 : 0;
        }
    }
}