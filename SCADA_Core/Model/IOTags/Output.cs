﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SCADA_Core.Model.IOTags
{
    public abstract class Output
    {
        public double InitialValue { get; set; }
        public double Value { get; set; }
        public abstract string ToStringWhenValueChanged(double oldValue);
    }
}