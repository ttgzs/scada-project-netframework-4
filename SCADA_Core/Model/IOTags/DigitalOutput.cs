﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using SCADA_Core.Interfaces;
using SCADA_Core.Model.IOTags;

namespace SCADA_Core.Model
{
    public class DigitalOutput : Output, ITag
    {
        public int TagName { get; set; }
        public string Description { get; set; }
        public char IOAdress { get; set; }

        public override string ToStringWhenValueChanged(double oldValue)
        {
            return $"Digital Tag {TagName} changed value from {oldValue} to {Value}.";
        }
    }
}