﻿using SCADA_Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SCADA_Core.Model
{
    [DataContract]
    public class AnalogueInput : Input, ITag, IAnalogue
    {
        [DataMember]
        public int TagName { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public char IOAdress { get; set; }
        [DataMember]
        public int LowLimit { get; set; }
        [DataMember]
        public int HighLimit { get; set; }
        [DataMember]
        public string Units { get; set; }
        public override double Read()
        {
            double retval = SimulationDriver.SimulationDriver.Addresses.Contains(IOAdress) ? 
                SimulationDriver.SimulationDriver.ReturnValue(IOAdress) : RealTimeDriver.Read(IOAdress);
            if (retval == -1)
            {
                return -1; // ERROR, nonexisting address in RealTimeDriver.
            }
            retval = retval > HighLimit ? HighLimit : retval;
            retval = retval < LowLimit ? LowLimit : retval;
            return Math.Round(retval, 2);
        }
    }
}