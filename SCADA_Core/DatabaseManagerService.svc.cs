﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using SCADA_Core.Model;
using SCADA_Core.ServiceContracts;
using System.Threading;

namespace SCADA_Core
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple), CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class DatabaseManagerService : TagProcessor, IDatabaseManager
    {

        public static event TagCollectionChanged OnTagCollectionChanged;

        public IDatabaseManagerCallback ManagerProxy
        {
            get
            {
                return OperationContext.Current.GetCallbackChannel<IDatabaseManagerCallback>();
            }
        }



        private DigitalInput AddDigitalInputTag(object obj)
        {
            DigitalInput di = (DigitalInput)obj;
            lock (digitalInputTagsLock)
            {
                digitalInputs.Add(di.TagName, di);
            }
            Thread t = new Thread(() => ThreadWork(di));
            lock (threadsLock)
            {
                tagThreads.Add(di.TagName, t);
            }
            t.Start();
            return di;
        }
        private AnalogueInput AddAnalogueInputTag(object obj)
        {
            AnalogueInput ai = (AnalogueInput)obj;
            lock (analogueInputTagsLock)
            {
                TagProcessor.analogueInputs.Add(ai.TagName, ai);
            }
            Thread t = new Thread(() => TagProcessor.ThreadWork(ai));
            lock (tagThreads)
            {
                tagThreads.Add(ai.TagName, t);
            }
            t.Start();
            return ai;
        }
        private void AddAnalogueOutputTag(AnalogueInput ai)
        {
            AnalogueOutput ao = new AnalogueOutput
            {
                TagName = ai.TagName,
                Description = $"This is output tag of analogue input tag {ai.TagName}, and output adress {ai.IOAdress}.",
                IOAdress = ai.IOAdress,
                LowLimit = ai.LowLimit,
                HighLimit = ai.HighLimit,
                Units = ai.Units,
                InitialValue = ai.LowLimit, // starting value
                Value = ai.LowLimit
            };
            lock (analogueOutputTagsLock)
            {
                analogueOutputs.Add(ao.TagName, ao);
            }
        }
        private void AddDigitalOutputTag(DigitalInput di)
        {
            DigitalOutput dou = new DigitalOutput
            {
                TagName = di.TagName,
                Description = $"This is output tag of digital input tag {di.TagName}, and output adress {di.IOAdress}.",
                IOAdress = di.IOAdress,
                InitialValue = 0, // starting value
                Value = 0
            };
            lock (digitalOutputTagsLock)
            {
                digitalOutputs.Add(dou.TagName, dou);
            }
        }
        public void AddTag(Input obj)
        {
            try
            {
                AnalogueInput ai = AddAnalogueInputTag(obj);
                AddAnalogueOutputTag(ai);
                
            } catch (InvalidCastException) 
            {
                try
                {
                    DigitalInput di = AddDigitalInputTag(obj);
                    AddDigitalOutputTag(di);
                } catch(InvalidCastException)
                {
                    return;
                }
            } catch (ArgumentException)
            {
                ManagerProxy.TagNameExistsCallback();
                return;
            }
            OnTagCollectionChanged();
        }

        public void ChangeOutputValue(int tagName, double newValue)
        {
            lock (analogueInputTagsLock) lock (digitalInputTagsLock) lock (analogueOutputTagsLock) lock (digitalOutputTagsLock)
            {
                if (analogueInputs.ContainsKey(tagName))
                {
                    double highLimit = analogueOutputs[tagName].HighLimit;
                    double lowLimit = analogueOutputs[tagName].LowLimit;
                    newValue = newValue > highLimit ? highLimit : newValue;
                    newValue = newValue < lowLimit ? lowLimit : newValue;
                    analogueOutputs[tagName].Value = Math.Round(newValue, 2);
                    ContextDataHandler.AddObservedTagValue(analogueInputs[tagName], newValue, tagName);
                }
                else if (digitalInputs.ContainsKey(tagName))
                {
                    digitalOutputs[tagName].Value = newValue > 0.5 ? 1 : 0;
                    ContextDataHandler.AddObservedTagValue(digitalInputs[tagName], newValue, tagName);
                }
            }
            UpdateTagXmlConfig();
                
        }
            
    

        public string GetOutputValue(int tagName)
        {
            lock (analogueInputTagsLock) lock (digitalInputTagsLock) lock (analogueOutputTagsLock) lock (digitalOutputTagsLock)
            {
                if (analogueInputs.ContainsKey(tagName))
                {
                    return analogueOutputs[tagName].Value.ToString();
                }
                else if (digitalInputs.ContainsKey(tagName))
                {
                    return digitalOutputs[tagName].Value.ToString();
                }
                else
                {
                    return "Invalid input.";
                }
            }
            
        }

        public void Login(string username, string password)
        {
            using (ContextDataHandler.UC = new UserContext())
            {
                User u = ContextDataHandler.UC.Users.Find(username);
                if (null != u)
                {
                    if (u.IsPasswordValid(password))
                    {
                        ManagerProxy.LoginCallback(true, u.Admin);
                    }
                }
            }
        }

        public void Logout() {}

        // TODO: Implement registration
        public bool Register(string username, string password)
        {
            string encryptedPassword = User.EncryptData(password);
            User user = new User
            {
                Username = username,
                Password = encryptedPassword,
                Admin = false
            };
            using (ContextDataHandler.UC = new UserContext())
            {
                try
                {
                    ContextDataHandler.UC.Users.Add(user);
                    ContextDataHandler.UC.SaveChanges();
                }
                catch (Exception)
                {
                    return false;
                }
            }
            return true;
        }

        public Dictionary<string, List<int>> GetTagNames()
        {
            lock (analogueInputTagsLock) lock (digitalInputTagsLock)
            {
                List<int> analogueTagNames = new List<int>(analogueInputs.Count);
                analogueTagNames.AddRange(analogueInputs.Keys);
                List<int> digitalTagNames = new List<int>(digitalInputs.Count);
                digitalTagNames.AddRange(digitalInputs.Keys);
                Dictionary<string, List<int>> tags = new Dictionary<string, List<int>>
                {
                    { "A", analogueTagNames },
                    { "D", digitalTagNames }
                };
                return tags;
            }
        }
        
        public void RemoveTag(int name)
        {
            lock (analogueInputTagsLock) lock (digitalInputTagsLock) lock (analogueOutputTagsLock) lock (digitalOutputTagsLock)
            lock (threadsLock)
            {
                if (analogueInputs.ContainsKey(name))
                {
                    tagThreads[name].Abort();
                    analogueInputs.Remove(name);
                    analogueOutputs.Remove(name);
                    tagThreads.Remove(name);
                }
                else if (digitalInputs.ContainsKey(name))
                {
                    tagThreads[name].Abort();
                    digitalInputs.Remove(name);
                    digitalOutputs.Remove(name);
                    tagThreads.Remove(name);
                } else { return; }
                UpdateTagXmlConfig();
            }
        }

        public void SwitchScanOnOff(int tagName, bool onoff)
        {
            lock (analogueInputTagsLock) lock (digitalInputTagsLock)
            {
                if (analogueInputs.ContainsKey(tagName))
                {
                    analogueInputs[tagName].OnOffScan = onoff;
                }
                else if (digitalInputs.ContainsKey(tagName))
                {
                    digitalInputs[tagName].OnOffScan = onoff;
                }
                UpdateTagXmlConfig();
            }
        }

        public void CreateAlarm(Alarm a)
        {
            lock (alarmsLock)
            {
                if (!alarms.ContainsKey(a.Name))
                {
                    alarms.Add(a.Name, new List<Alarm>());
                }
                alarms[a.Name].Add(a);
            }
            XmlDataIO.WriteAlarmsToXmlConfig();
        }

        public bool RemoveAlarm(int name, int threshold)
        {
            bool success = false;
            lock (alarmsLock)
            {
                if (!alarms.ContainsKey(name))
                {
                    success = false;
                }
                else
                {
                    foreach (Alarm a in alarms[name])
                    {
                        if (a.Threshold == threshold)
                        {
                            alarms[name].Remove(a);
                            success = true;
                            break;
                        }
                    }
                }
            }
            if (success) 
                XmlDataIO.WriteAlarmsToXmlConfig();
            return success;
        }
    }
}
