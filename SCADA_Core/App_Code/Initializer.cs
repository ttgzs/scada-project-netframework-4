﻿using SCADA_Core.Model;
using System.Linq;

namespace SCADA_Core.App_Code
{
    public class Initializer : ContextDataHandler
    {
        public static void AppInitialize()
        {
            TagProcessor.InitializeTagCollections();
            TagProcessor.InitializeDelegates();
            InitializeTagCollections();
            InitializeAlarmsCollection();

            ContextDataHandler.InitializeUserDb();
            ClearTagDb();
            TagProcessor.StartThreads();
        }

        private static void ClearTagDb()
        {
            TC = new TagDataContext();
            TC.TagData.RemoveRange(TC.TagData.Where(a => a.TagName == 1 || a.TagName == 3 || a.TagName == 6));
            TC.SaveChanges();
            TC.Dispose();
        }

        private static void InitializeTagCollections()
        {
            XmlDataIO.ReadTagXmlConfig();
        }

        private static void InitializeAlarmsCollection()
        {
            XmlDataIO.ReadAlarmXmlConfig();
        }
    }
}