﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AlarmDisplay
{
    class AlarmDisplayCallback : ServiceReference.IAlarmDisplayCallback
    {
        public void DisplayAlarm(string message, int repeat)
        {
            Console.WriteLine(string.Concat(Enumerable.Repeat("=", message.Length)));
            for (int i = 0; i < repeat; i++)
            {
                Console.WriteLine(message);
            }
            
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            AlarmDisplayCallback adcb = new AlarmDisplayCallback();
            InstanceContext ic = new InstanceContext(adcb);
            ServiceReference.AlarmDisplayClient adc = new ServiceReference.AlarmDisplayClient(ic);
            adc.ConnectToService();
            while (true) { }
        }
    }
}
