﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCADA_Core.Model;

namespace DatabaseManager
{
    class ManagerOptionMenu
    {

        private static string ChooseTagType()
        {
            Console.Write("Choose Tag type (D/A): ");
            return Console.ReadLine();
        }

        public static Input AddTag()
        {
            string tagtype = ChooseTagType();
            if (tagtype == "A")
            {
                return TagCreationMenu.CreateAnalogueInputTag();
            } else if (tagtype == "D")
            {
                return TagCreationMenu.CreateDigitalInputTag();
            }
            return null;
        }

        public static int PickTag(Dictionary<string, int[]> tagNames)
        {
            StringBuilder options = new StringBuilder();
            options.Append("\n\nPick tag\n");
            AppendDigitalTags(tagNames, options);
            AppendAnalogueTags(tagNames, options);
            options.Append("\n");
            Console.Write(options.ToString());
            int chosen = int.Parse(Console.ReadLine());
            while (!tagNames["A"].Contains(chosen) && !tagNames["D"].Contains(chosen))
            {
                Console.WriteLine("Invalid tag. Pick again.");
                chosen = int.Parse(Console.ReadLine());
            }
            return chosen;
        }

        private static void AppendAnalogueTags(Dictionary<string, int[]> tagNames, StringBuilder options)
        {
            options.AppendLine("\n=============");
            options.AppendLine("Analogue tags:");
            for (int j = 0; j < tagNames["A"].Length; j++)
            {
                options.Append($"{tagNames["A"][j]}, ");
            }
        }

        private static void AppendDigitalTags(Dictionary<string, int[]> tagNames, StringBuilder options)
        {
            
            options.AppendLine("============");
            options.AppendLine("Digital tags:");
            for (int i = 0; i < tagNames["D"].Length; i++)
            {
                options.Append($"{tagNames["D"][i]}, ");
            }
        }

        public static bool SwitchScanOnOff()
        {
            Console.Write("Turn scan on or off (o/f): ");
            string option = Console.ReadLine();
            return option == "o";
        }
        public static double EnterNewValue()
        {
            Console.Write("Enter new value: ");
            return double.Parse(Console.ReadLine());
        }
    
    
        public static Alarm CreateAlarm(Dictionary<string, int[]> tagNames)
        {
            StringBuilder options = new StringBuilder();
            options.Append("\n\nPick tag\n");
            AppendAnalogueTags(tagNames, options);
            Console.WriteLine(options.ToString());
            int chosen = int.Parse(Console.ReadLine());
            while (!tagNames["A"].Contains(chosen))
            {
                Console.WriteLine("Invalid tag. Pick again.");
                chosen = int.Parse(Console.ReadLine());
            }

            return AlarmCreateMenu.CreateAlarm(chosen);
        }
    
        public static void RemoveAlarm(Dictionary<string, int[]> tagNames, ServiceReference.DatabaseManagerClient dmc)
        {
            StringBuilder options = new StringBuilder();
            options.Append("\n\nPick tag\n");
            AppendAnalogueTags(tagNames, options);
            Console.WriteLine(options);

            int chosen = int.Parse(Console.ReadLine());
            while (!tagNames["A"].Contains(chosen))
            {
                Console.WriteLine("Invalid tag. Pick again.");
                chosen = int.Parse(Console.ReadLine());
            }
            int threshold = AlarmCreateMenu.InputThreshold();
            if (dmc.RemoveAlarm(chosen, threshold))
            {
                Console.WriteLine("Alarm successfully removed.");
            } else
            {
                Console.WriteLine($"Tag has no alarms with threshold {threshold}. No alarms removed.");
            }
        }
    
        public static string InputRegistrationCredentials(string flavortext)
        {
            Console.Write(flavortext);
            return Console.ReadLine();

        }
    }
}
