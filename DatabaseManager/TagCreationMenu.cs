﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCADA_Core.Model;

namespace DatabaseManager
{
    class TagCreationMenu
    {
        // Input abstract class
        private static bool OnOffScan { get; set; }
        private static int ScanTime { get; set; }

        // ITag interface
        private static int TagName { get; set; }
        private static string Description { get; set; }
        private static char IOAdress { get; set; }

        // IAnalogue interface
        private static int LowLimit { get; set; }
        private static int HighLimit { get; set; }
        private static string Units { get; set; }

        public static AnalogueInput CreateAnalogueInputTag()
        {
            InputGenericTagFields();
            InputAnalogueTagFields();
            InputInputTagFields();
            return new AnalogueInput {
                OnOffScan = OnOffScan,
                ScanTime = ScanTime,
                TagName = TagName,
                Description = Description,
                IOAdress = IOAdress,
                LowLimit = LowLimit,
                HighLimit = HighLimit,
                Units = Units
            };
        }

        public static DigitalInput CreateDigitalInputTag()
        {
            
            InputGenericTagFields();
            InputInputTagFields();
            return new DigitalInput
            {
                OnOffScan = OnOffScan,
                ScanTime = ScanTime,
                TagName = TagName,
                Description = Description,
                IOAdress = IOAdress
            };
        }

        private static void InputGenericTagFields()
        {
            Console.Write("New tag name (int): ");
            TagName = int.Parse(Console.ReadLine());
            Console.Write("Description: ");
            Description = Console.ReadLine();
            IOAdress = 'f';
            Console.Write("New tag address: ");
            IOAdress = Console.ReadKey().KeyChar;
            
            
        }   
        private static void InputAnalogueTagFields()
        {
            Console.Write("New tag low limit (0-100): ");
            LowLimit = Math.Abs(int.Parse(Console.ReadLine()));
            Console.Write("New tag high limit (0-100): ");
            HighLimit = Math.Abs(int.Parse(Console.ReadLine()));
            Console.Write("New tag units: ");
            Units = Console.ReadLine();
        }
        private static void InputInputTagFields()
        {
            Console.Write("New tag scan (y/N): ");
            string i = Console.ReadLine();
            OnOffScan = "y" == i;
            Console.Write("New tag scantime (miliseconds): ");
            ScanTime = Math.Abs(int.Parse(Console.ReadLine()));
        }
    }
}
