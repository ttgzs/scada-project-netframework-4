﻿using SCADA_Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseManager
{
    // CRUD-create read update delete
    // AlarmCreateDeleteMenu
    class AlarmCreateMenu
    {
        private static readonly int[] priorities = { 1, 2, 3 };
        // Alarm fields

        public static Alarm CreateAlarm(int tagName)
        {
            int t = InputThreshold();
            int p = InputPriority();
            Alarm.AlarmType type = InputType();
            Alarm a = new Alarm
            {
                Name = tagName,
                Threshold = t,
                Priority = p,
                Type = type,

            };
            return a;
        }

        public static Alarm.AlarmType InputType()
        {
            Console.Write("Input alarm type (LOWER/HIGHER): ");
            string input = "";
            while (input != "LOWER" && input != "HIGHER")
            {
                input = Console.ReadLine();
            }
            return input == "LOWER" ? Alarm.AlarmType.LOWER : Alarm.AlarmType.HIGHER;

        }

        public static int InputPriority()
        {
            Console.Write("Input alarm priority (1, 2, 3): ");
            string input = "";
            while (input != "1" && input != "2" && input != "3")
            {
                input = Console.ReadLine();
            }
            return int.Parse(input);
        }

        public static int InputThreshold()
        {
            Console.Write("Input alarm threshold: ");
            int retval = -1;
            while (retval < 0)
            {
                try
                {
                    retval = int.Parse(Console.ReadLine());
                } catch (Exception)
                {
                    continue;
                }
            }
            return retval;

        }
    }
}
