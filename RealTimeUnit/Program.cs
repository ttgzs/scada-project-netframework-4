﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RealTimeUnit
{
    class Program
    {
        private static CspParameters csp = new CspParameters();
        private static RSACryptoServiceProvider rsa;
        // --------------------------------------------------------------.../RealTimeUnit/Bin/Debug/RealTimeUnit.exe
        // --------------------------------------------------------------.../Scada_Core/PublicKeys/
        private static readonly string PUBLIC_KEY_DIR = Path.GetFullPath("../../../SCADA_Core/PublicKeys/");
        private static readonly string KEY_STORE = "MY_KEY_STORE";


        static void Main(string[] args)
        {
            ServiceReference.RTUClient rtuc = new ServiceReference.RTUClient();
            Console.WriteLine(PUBLIC_KEY_DIR);
            if (!Directory.Exists(PUBLIC_KEY_DIR))
            {
                Directory.CreateDirectory(PUBLIC_KEY_DIR);
            }

            char address = DefineAddress();
            int lowLimit = 100;
            int highLimit = 0;
            int sleepTime = 1000;
            while (lowLimit > highLimit || sleepTime < 0 || lowLimit < 0 || highLimit < 0)
            {
                lowLimit = InputIntValue("Define low limit: ");
                highLimit = InputIntValue("Define high limit: ");
                sleepTime = InputIntValue("Define sleep time (miliseconds): ");
            }
            if (rtuc.SignUp(address))
            {
                string id = rtuc.GetId(address);
                var random = new Random();
                string PUBLIC_KEY_PATH = PUBLIC_KEY_DIR + id + ".xml";
                CreateAsmKeys(false);
                WritePublicKey(PUBLIC_KEY_PATH);
                rtuc.ExportPublicKey(PUBLIC_KEY_PATH, id);

                while (true)
                {
                    double newValue = random.NextDouble() * (highLimit - lowLimit) + lowLimit;
                    string message = newValue.ToString();
                    Console.WriteLine("Message sent: " + message);
                    byte[] hashValue = SignMessage(message);
                    rtuc.SendMessage(id, address, message, hashValue);
                    
                    Thread.Sleep(sleepTime);
                }
            }
            else
            {
                Console.WriteLine("Address is taken.");
                Console.ReadLine();
            }
        }

        private static byte[] SignMessage(string message)
        {
            using (SHA256 sha = SHA256Managed.Create())
            {
                byte[] hashValue = sha.ComputeHash(Encoding.UTF8.GetBytes(message));
                CspParameters csp = new CspParameters();
                csp.KeyContainerName = KEY_STORE;
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(csp);
                var formatter = new RSAPKCS1SignatureFormatter(rsa);
                formatter.SetHashAlgorithm("SHA256");
                return formatter.CreateSignature(hashValue);
            }
        }


        private static void CreateAsmKeys(bool useMachineKeyStore)
        {
            csp.KeyContainerName = KEY_STORE;
            if (useMachineKeyStore)
                csp.Flags = CspProviderFlags.UseMachineKeyStore;
            rsa = new RSACryptoServiceProvider(csp);
            rsa.PersistKeyInCsp = true;
        }
        private static void WritePublicKey(string keyPath)
        {
            StreamWriter textWriter = new StreamWriter(keyPath, false, Encoding.UTF8);
            textWriter.Write(rsa.ToXmlString(false));
            textWriter.Flush();
            textWriter.Close();
        }

        private static char DefineAddress()
        {
            char address =  'S';
            char[] forbbiden = new char[] { 'S', 'C', 'R' };

            while (forbbiden.Contains(address))
            {
                Console.Write("Define address for RealTimeDriver (do not use S/C/R): ");
                address = Console.ReadKey().KeyChar;
            }
            Console.WriteLine();
            return address;
        }
        static public int InputIntValue(string message)
        {
            int retval;
            Console.Write(message);

            while (true)
            {
                try
                {
                    retval = int.Parse(Console.ReadLine());
                    break;
                } catch (Exception)
                {
                    Console.WriteLine("Invalid input.");
                    continue;
                }
            }
            return retval;
        }
    }
}
